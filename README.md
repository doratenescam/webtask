## Technologies
***
Technologies used within the project:
* [react-animated-css](https://www.npmjs.com/package/react-animated-css)
* [react-bootstrap](react-bootstrap.github.io)
* [react-icons](https://react-icons.github.io/react-icons/)
* [react-number-format](https://www.npmjs.com/package/react-number-formate)
* [react-responsive](https://www.npmjs.com/package/react-responsive)
* [react-router-dom](https://www.npmjs.com/package/react-router-dom)
* [styled-components](https://styled-components.com)


## Installation
***
1. git clone https://gitlab.com/doratenescam/webtask.git
2. cd ../path/to/the/file
3. npm install
4. Config: 
   - BASE_URL  : at src/utils/constant.js     url for the API request
5. npm start --port 3001
    - To consume the API at the same PC, start this project in other port --port 3001
6. Open http://127.0.0.1:3001/#/ at your browser.

## Webtask

Webtask project (Reactjs)
- Home: list of cars, getting info from webtaskAPI  http://127.0.0.1:3001/#/
- Detail: car detail, getting info from webtaskAPI  http://127.0.0.1:3001/#/car-detail
