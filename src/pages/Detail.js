import React from "react";
import Features from '../components/detail/Features'
import Header from '../components/detail/Header'
import DetailProvider from "../context/DetailProvider"

function Detail() {  
  
  return ( 
      <DetailProvider> 
        <div className="d-flex w-100"  style={{flexWrap: 'wrap'}}>
          <Header/>
        </div>
        <div className="d-md-flex d-lg-flex w-100">
          <Features data="exterior"/>
          <Features data="performance" second={true}/>
        </div>
      </DetailProvider>
  );
}

export default Detail;