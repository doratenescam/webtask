import {BASE_URL} from '../utils/constant';
export async function getCarsApi(){
    try {
        const url=`${BASE_URL}/cars`
        const reponse = await fetch(url)
        const result = await reponse.json()
        return result
    } catch (error) {
        console.log("error")
        return null
    }
}