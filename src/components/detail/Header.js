import React from "react"
import { useMediaQuery } from "react-responsive"
import GalleryMobile from "./Gallery/GalleryMobile"
import GalleryDesktop from "./Gallery/GalleryDesktop"
import Detail from "./Detail"
import {Btn} from "./styles.js"

function Header(props){
    const isMobile = useMediaQuery({ maxWidth: 464 })

    return(
        <>
        {isMobile &&
            <>
            <GalleryMobile/>
            <Detail isMobile={isMobile}/>
            <Btn> CALL US</Btn>
            </>
        }

        {!isMobile &&
            <>
                <Detail isMobile={isMobile}/>   
                <GalleryDesktop/>
            </>
        }
        </>
)
}

export default Header