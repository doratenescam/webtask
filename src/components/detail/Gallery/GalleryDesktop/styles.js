import styled from "styled-components"

export const BigImage = styled.div`
    height: 100%;
    width: auto;
    min-width: 300px;
    background-image: url(${props => (props.image? props.image: '')});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

export const Item = styled.div`
    height: 123px;
    width: 16.63%;
    cursor:pointer;
    background-image: url(${props => (props.image? props.image: '')});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;
