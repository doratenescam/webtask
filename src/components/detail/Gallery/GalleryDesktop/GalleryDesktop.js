import React, {useState, useContext} from "react"
import {Animated} from "react-animated-css"
import {Col} from "react-bootstrap"
import {Item, BigImage} from "./styles"
import { DetailContext } from "../../../../context/DetailProvider"

function GalleryDesktop(props){
    const { images, thumbnailImages } = useContext(DetailContext)/*Context*/
    const [image, setImage]= useState(0)
    const [isVisible, setIsVisible]= useState(true)

    const hanldClick = (imageNew) => {
        setIsVisible(false)

        setTimeout(()=>{
            setImage(imageNew)
            setIsVisible(true)
        }, 400);
    }

    const menuImages= thumbnailImages.map((url, idx)=>{
        return <Item image={url} onClick={()=>hanldClick(idx)} key={`item${idx}`}/>
    })
    return(
        <>
        <Col  lg="8" md="8" className="order-first px-0">
            <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={800} isVisible={isVisible} style={{height: "100%"}}>
                <BigImage image={images[image]}/>              
            </Animated>
        </Col>
        <Col  lg="12" className="order-last px-0 mt-4">
            <div className="d-flex flex-row img-line second px-0">
                {menuImages}
            </div>
        </Col>
        </>
    )
}

export default GalleryDesktop