import styled from "styled-components"

export const Image = styled.div`
    position: relative;
    width: auto;
    height: 256px;
    background-image: url(${props => (props.src? props.src: '')});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

