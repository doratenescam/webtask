import React, { useContext} from "react"
import {Carousel} from "react-bootstrap"
import {Image} from "./styles"
import { DetailContext } from "../../../../context/DetailProvider";

function GalleryMobile(){
    const { images } = useContext(DetailContext)/*Context*/

    const items= images.map((url, idx)=>{
        return <Carousel.Item interval={1000} key={`img${idx}`}>
                    <Image src={url}/>
                </Carousel.Item>
    })

    return(
        <Carousel controls={false} pause="false" className="w-100">
            {items}
        </Carousel>
    )
}

export default GalleryMobile