import React, { useContext} from "react"
import {Col, Row} from "react-bootstrap"
import { FaEnvelope  } from 'react-icons/fa'
import NumberFormat from 'react-number-format'
import Counter from './Counter';
import { DetailContext } from "../../../context/DetailProvider";
import {Container, Title, Label, Data, Extra, Share, Info} from "./styles"

function Detail(props){
    const { carInfo } = useContext(DetailContext)/*Context*/

    return(
        <Col  lg="4" md="4" sm="12" className="px-0 bg-white">
            <Title>{carInfo.name}</Title>
            <Container>                
                <Info>                  
                    <Label>Year</Label>
                    <Data>{carInfo.year}</Data>

                    {carInfo.priceRange &&
                    <>
                        <Label>Price range</Label>
                        <Data>
                            <NumberFormat value={carInfo.priceRange.from} thousandSeparator={true}  displayType={'text'} prefix={'$'} /> 
                             - 
                            <NumberFormat value={carInfo.priceRange.to} thousandSeparator={true}  displayType={'text'} prefix={'$'} />
                        </Data>
                    </>
                    }

                    <Label>Mileage</Label>
                    <Data last>
                    <NumberFormat value={carInfo.mileage} thousandSeparator={true}  displayType={'text'} /> miles
                    </Data>
                </Info>

                <Info secondary>
                    <Extra>Item Number: {carInfo.itemNumber}</Extra >
                    <Extra >Vin: {carInfo.vin}</Extra>

                    <Share>Share this car <FaEnvelope/></Share >

                    <Row className="mx-0">
                        <Counter label="Views" value={carInfo.views}/>
                        {!props.isMobile &&
                        <>
                            <Counter label="Saves" value={carInfo.saves}/>
                            <Counter label="Views" value={carInfo.shares}/>
                        </>}
                    </Row>
                </Info>
            </Container>
        </Col>
)
}

export default Detail