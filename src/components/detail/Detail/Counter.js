import {Col} from "react-bootstrap"
import NumberFormat from 'react-number-format'
import { Label,Cont} from "./styles"

function Counter(props){
    return(
        <Col className="px-0">
            <Label cont>{props.label}</Label>
            <Cont>
            <NumberFormat value={props.value} thousandSeparator={true}  displayType={'text'}/>
            </Cont>
        </Col>
    )
}

export default Counter