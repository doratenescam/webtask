import styled from "styled-components"

export const Container = styled.div`
  width:100%;
  text-align: left;
  position: relative;
  padding-left:40px;
  @media (max-width: 464px) {
    display: flex;
    align-items: flex-end;
    flex-wrap: nowrap;
    padding: 0px 15px 32px 15px;
  }
`;

export const Info= styled.div`
    flex: 100%;
    @media (max-width: 464px) {
      flex: ${props => (props.secondary ? "50%":"50%")};
    }
`;

export const Title = styled.h1`
  font-size: 36px;
  font-weight: 700;
  line-height: 36px;
  color: #343434;
  text-align: left;
  margin: 48px 0px 32px 40px;
  @media (max-width: 464px) {
    margin: 32px 0px 16px 30px;
  }
`;

//h5
export const Label = styled.label`
  font-size: 14px;
  font-weight: 600;
  line-height: 14px;
  color: #9b9b9b;
  display: block;
  margin-bottom: ${props => (props.cont ? "0px":"8px")};
`;

export const Data = styled.label`
  font-size: 20px;
  font-weight: 700;
  line-height: 20px;
  color: #343434;
  display: block;
  margin-bottom: ${props => (props.last ? "32px":"16px")};
  @media (max-width: 464px) {
    font-size: 18px;
    line-height: 18px;
  }
`;

export const Extra = styled.span`
  font-size: 14px;
  font-weight: 600;
  line-height: 14px;
  color: #9b9b9b;
  margin-bottom: 8px;
  display: block;
`;

export const Share = styled.label`
  font-size: 14px;
  line-height: 14px;
  font-weight: 500;
  color: #343434;
  margin-bottom: 32px;
  margin-top: 32px;
  @media (max-width: 464px) {
    margin: 24px 0px;
  }
`;

export const Cont = styled.label`
  font-size: 18px;
  line-height: 18px;
  font-weight: 500;
  color: #3FB34A;
  margin-bottom: 32px;
  @media (max-width: 464px) {
    
  }
`;

