import React from "react"
import {Label, Text, Line} from "./styles"

function Item(props){
    return(
        <Line>
            <Label>{props.label}</Label>
            <Text>{props.text}</Text>
        </Line>
    )
}

export default Item