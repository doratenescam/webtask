import React, { useContext} from "react"
import {Col} from "react-bootstrap"
import {Container, Title} from "./styles"
import Item from "./Item"
import { useMediaQuery } from "react-responsive"
import { DetailContext } from "../../../context/DetailProvider";

function Features(props){
    const { exterior, performance } = useContext(DetailContext)/*Context*/
     let data=exterior
     if(props.data==='performance')data=performance

    const isMobile = useMediaQuery({ maxWidth: 464 })
    let margin="24px 4px 24px 32px"
    if(props.second)margin="24px 32px 24px 4px"
    if(isMobile)margin="10px 0px"

    return(
        <Col sm="12" lg="6" md="6" className="px-0">
            <Container margin={margin }>
                <Title>{props.data}</Title>
                <Item label="Cylinders" text={data.cylinders}/>
                <Item label="Cyti MPG" text={data.cityMpg}/>
                <Item label="Highway MPG" text={data.highwayMpg}/>
                <Item label="Engine" text={data.engine}/>
            </Container>
        </Col>
)
}

export default Features

