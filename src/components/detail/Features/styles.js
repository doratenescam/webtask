import styled from "styled-components"

export const Container = styled.div`
  width:auto;
  margin: ${props => (props.margin ? props.margin: '')};
  padding: 40px;
  background-color: #FFFFFF;
  text-align: left;
`;

export const Title = styled.h1`
  font-size: 20px;
  font-weight: 700;
  line-height: 18px;
  color: #9B9B9B;
  margin-bottom: 24px;
  text-transform: uppercase;
`;

//Item feature
export const Label = styled.label`
  font-size: 14px;
  font-weight: 500;
  line-height: 14px;
  color: #9b9b9b;
  float: left;
  flex: 70%;
`;

export const Text = styled.span`
  font-size: 20px;
  font-weight: 500;
  line-height: 20px;
  color: #343434;
  flex: 30%;
`;

export const Line = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 40px;
  border-bottom: 2px solid #9b9b9b47;
  margin: 12px 0px;
  position: relative;
  justify-content: center;
  align-items: center;
`;