import styled from "styled-components"

export const Btn = styled.button`
  font-size: 18px;
  font-weight: 700;
  line-height: 18px;
  color: #FFFFFF;
  width:100%;
  height: 40px;
  background-color: #3FB34A;
  border: 1px solid #3FB34A;
  margin: 16px 0px;
`;
