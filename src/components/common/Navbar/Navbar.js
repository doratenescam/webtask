import React from "react"
import {Navbar, Nav} from "react-bootstrap"
import { useMediaQuery } from "react-responsive"
import { FaHome, FaSearch, FaPhone, FaClock  } from 'react-icons/fa'
import { Link  } from "react-router-dom"

function NavbarDesktop(){
    const isMobile = useMediaQuery({ maxWidth: 464 })

    return(
        <Navbar collapseOnSelect expand={true} className="nav-bar" variant="dark">
            <Nav className="ml-auto">
                {!isMobile &&
                <Nav.Link href="#" className="first">
                    <FaSearch/>
                </Nav.Link>}
                
                
                <Nav.Link href="/">
                    <Link as={Link} to='/'>
                        <FaHome/>
                    </Link>
                </Nav.Link>

                <Nav.Link href="#">
                    <FaPhone/>
                </Nav.Link>
                {!isMobile &&
                <Nav.Link href="#" className="last">
                    <FaClock/>
                </Nav.Link>}
            </Nav>
        </Navbar>
    )
}

export default NavbarDesktop