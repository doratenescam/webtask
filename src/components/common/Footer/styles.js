import styled from "styled-components"

export const Button = styled.div`
  width:110px;
  height :auto;
  margin:10px 0px;
  text-align: center;
  justify-content: center;
  align-items: center;
  position: relative;
  display:inline-block;
  padding: 0px 5px;

  -ms-transform: skewX(-20deg);
  -webkit-transform: skewX(-20deg);
  transform: skewX(-20deg); 

  border-left:  ${props => (props.middle ? "1px solid #ffffff":"unset")};
  border-right:  ${props => (props.middle ? "1px solid #ffffff":"unset")};
`;

export const TexBtn = styled.span`
  font-size:16px;
  line-height:16px;
  color: #9B9B9B;
  -ms-transform: skewX(-20deg);
  -webkit-transform: skewX(-20deg);
  transform: skewX(-20deg); 
`;
export const Text = styled.label`
  font-size:14px;
  font-weight: 600;
  color: #9B9B9B;
  display:block;
`;
