import React from "react"
import {Button, Text, TexBtn } from './styles.js'
import { Link } from "react-router-dom"

function Footer(){
    return(
        
        <div className="footer w-100 bg-black ">
            <div className="row justify-content-md-center mx-0">

                <div className="d-flex justify-content-center">
                    <Button>
                        <TexBtn>About us</TexBtn>
                    </Button>
                    
                    <Link as={Link} to='/'>
                        <Button middle>
                            <TexBtn>Home</TexBtn>
                        </Button>
                    </Link>
                    
                    <Button>
                        <TexBtn>Privacy Policy</TexBtn>
                    </Button>
                </div>
                <div className="col-12 text-center">
                    <Text>PLS is a regiter service mark and other marks are service mars of PLS</Text>
                    <Text>Financial Services, Inc. ©2021</Text>
                </div>   
                
            </div>
        </div>
        )
}

export default Footer;