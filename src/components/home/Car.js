import React from "react"
import { Link } from "react-router-dom"
import { Label, Data, Title, BigImage} from "./styles"
import {Col} from "react-bootstrap"
import NumberFormat from 'react-number-format'

function Car({data}){
    const handleClick=()=>window.sessionStorage.setItem("id", data._id);
    return(
        <Col lg="6" md="6" sm="12" className="text-center">
            <Link as={Link} to='/car-detail' onClick={()=>handleClick()}>
                <BigImage image={data.images[0]}/>
            </Link>

            
            <div className="bg-white pb-3 pt-1">
                <Link as={Link} to='/car-detail' onClick={()=>handleClick()}>
                    <Title>{data.name}</Title>
                </Link>
                {data.priceRange &&
                <>
                    <Label>Price range</Label>
                    <Data>
                        <NumberFormat value={data.priceRange.from} thousandSeparator={true}  displayType={'text'} prefix={'$'} /> 
                            - 
                        <NumberFormat value={data.priceRange.to} thousandSeparator={true}  displayType={'text'} prefix={'$'} />
                    </Data>
                </>
                }
            </div>
        </Col>
    )
}

export default Car