import React,{useState,useEffect} from "react"
import {getCarsApi} from '../../api/cars'
import Car from './Car'

function CarList(props){
    const [list, setList] = useState([])

    useEffect(() => {
        (async () => {
            const response = await getCarsApi()        
            setList(response)
        })()
    }, []);

    const carsItem= list.map((item, idx)=><Car data={item} key={`car${idx}`}/>)

    return(
        <>
        {carsItem}
        </>
)
}

export default CarList