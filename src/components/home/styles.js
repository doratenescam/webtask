import styled from "styled-components"

export const BigImage = styled.div`
    height: 300px;
    width: auto;
    min-width: 300px;
    background-image: url(${props => (props.image? props.image: '')});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

export const Title = styled.h1`
  font-size: 36px;
  font-weight: 700;
  line-height: 36px;
  color: #343434;
  margin: 48px 0px 32px 40px;
  @media (max-width: 464px) {
    margin: 32px 0px 16px 30px;
  }
`;

//h5
export const Label = styled.label`
  font-size: 14px;
  font-weight: 500;
  line-height: 14px;
  color: #9b9b9b;
  display: block;
  margin-bottom: ${props => (props.cont ? "0px":"8px")};
`;

export const Data = styled.label`
  font-size: 20px;
  font-weight: 700;
  line-height: 20px;
  color: #343434;
  display: block;
  margin-bottom: ${props => (props.last ? "32px":"16px")};
`;

