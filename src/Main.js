import './index.css';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Component } from "react"
import {
  Route,
  HashRouter
} from "react-router-dom"

import Home from "./App"
import Detail from "./pages/Detail"

import Navbar from './components/common/Navbar'
import Footer from './components/common/Footer'

class Main extends Component {
  render() {
    return (
        <HashRouter nUpdate={() => window.scrollTo(0, 0)}>
          <div>
              <Navbar/>
              <div className="content" id="content-template">
                  <Route exact path="/" component={Home}/>
                  <Route path="/car-detail" component={Detail}/>
              </div>   
              <Footer/>         
          </div>
        </HashRouter>
    );
  }
}
 
export default Main;