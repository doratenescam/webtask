import React, { createContext, useState, useEffect } from "react"
import {getCarByidApi} from '../api/carByid'
import { useHistory } from "react-router-dom"

export const DetailContext = createContext();

const DetailProvider = ({ children }) => {
  const [carInfo, setCarInfo] = useState([])
  const [images, setImages] = useState([])
  const [thumbnailImages, setThumbnailImages] = useState([])
  const [exterior, setExterior] = useState([])
  const [performance, setPerformance] = useState([])
  let history = useHistory()

  /*Get car data from API*/
  useEffect(() => {
    (async () => {
        let id= ''
        if(window.sessionStorage.getItem("id"))id= window.sessionStorage.getItem("id")
        else history.push("/")

        const response = await getCarByidApi(id)        
        
        setCarInfo(response)
        setImages(response.images)
        setThumbnailImages(response.thumbnailImages)
        setExterior(response.exterior)
        setPerformance(response.performance)
    })()
  }, []);
  
  return (
    <DetailContext.Provider value={{ 
      carInfo,
      images,
      thumbnailImages,
      exterior,  
      performance}}>
      {children}
    </DetailContext.Provider>
  );
};

export default DetailProvider;