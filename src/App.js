import CarList from './components/home/CarList'
import {Row, Container} from "react-bootstrap"

function App() {  
  return (
    <div className="App"> 
      <Container >
        <Row className="py-5" style={{height: "83vh"}}>
          <CarList/>
        </Row>
      </Container>
    </div>
  );
}

export default App;
